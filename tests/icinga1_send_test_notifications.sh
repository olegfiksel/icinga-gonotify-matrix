#!/bin/bash

LONGDATETIME=$(date)
HOSTNAME=$(hostname)
HOSTDISPLAYNAME=$(hostname -f)
SERVICENAME='CI Pipeline'
SERVICEDISPLAYNAME='CI Pipeline'
HOSTOUTPUT='Lorem ipsum dolor sit amet,
Odio et placerat lacinia, arcu.
'

MATRIXNOTIFYBIN='./target/icinga-gonotify-matrix-linux-amd64'

MATRIXCONFIG='config.yaml'

# Host notification
TEMPLATE=./icinga1/matrix-host-notification.sh
## DOWN
$TEMPLATE \
    -d "$LONGDATETIME" \
    -l "$HOSTNAME" \
    -n "$HOSTDISPLAYNAME" \
    -o "$HOSTOUTPUT" \
    -s "DOWN" \
    -t "PROBLEM" \
    -m "$MATRIXNOTIFYBIN" \
    -x "$MATRIXCONFIG"
## UNREACHABLE
$TEMPLATE \
    -d "$LONGDATETIME" \
    -l "$HOSTNAME" \
    -n "$HOSTDISPLAYNAME" \
    -o "$HOSTOUTPUT" \
    -s "UNREACHABLE" \
    -t "PROBLEM" \
    -m "$MATRIXNOTIFYBIN" \
    -x "$MATRIXCONFIG"
## UP
$TEMPLATE \
    -d "$LONGDATETIME" \
    -l "$HOSTNAME" \
    -n "$HOSTDISPLAYNAME" \
    -o "$HOSTOUTPUT" \
    -s "UP" \
    -t "RECOVERY" \
    -m "$MATRIXNOTIFYBIN" \
    -x "$MATRIXCONFIG"

# Service notification
TEMPLATE=./icinga1/matrix-service-notification.sh
## WARNING
$TEMPLATE \
    -d "$LONGDATETIME" \
    -e "$SERVICENAME" \
    -l "$HOSTNAME" \
    -n "$HOSTDISPLAYNAME" \
    -o "$HOSTOUTPUT" \
    -s "WARNING" \
    -t "PROBLEM" \
    -u "$SERVICEDISPLAYNAME" \
    -m "$MATRIXNOTIFYBIN" \
    -x "$MATRIXCONFIG"
## CRITICAL
$TEMPLATE \
    -d "$LONGDATETIME" \
    -e "$SERVICENAME" \
    -l "$HOSTNAME" \
    -n "$HOSTDISPLAYNAME" \
    -o "$HOSTOUTPUT" \
    -s "CRITICAL" \
    -t "PROBLEM" \
    -u "$SERVICEDISPLAYNAME" \
    -m "$MATRIXNOTIFYBIN" \
    -x "$MATRIXCONFIG"
## UNKNOWN
$TEMPLATE \
    -d "$LONGDATETIME" \
    -e "$SERVICENAME" \
    -l "$HOSTNAME" \
    -n "$HOSTDISPLAYNAME" \
    -o "$HOSTOUTPUT" \
    -s "UNKNOWN" \
    -t "PROBLEM" \
    -u "$SERVICEDISPLAYNAME" \
    -m "$MATRIXNOTIFYBIN" \
    -x "$MATRIXCONFIG"
## OK
$TEMPLATE \
    -d "$LONGDATETIME" \
    -e "$SERVICENAME" \
    -l "$HOSTNAME" \
    -n "$HOSTDISPLAYNAME" \
    -o "$HOSTOUTPUT" \
    -s "OK" \
    -t "RECOVERY" \
    -u "$SERVICEDISPLAYNAME" \
    -m "$MATRIXNOTIFYBIN" \
    -x "$MATRIXCONFIG"
