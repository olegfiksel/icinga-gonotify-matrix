module github.com/oleg-fiksel/icinga-gonotify-matrix

go 1.14

require (
	github.com/ahmetb/govvv v0.3.0 // indirect
	github.com/go-playground/validator/v10 v10.2.0
	github.com/sirupsen/logrus v1.4.2
	gopkg.in/yaml.v2 v2.2.8
	maunium.net/go/mautrix v0.1.0-alpha.2
)
