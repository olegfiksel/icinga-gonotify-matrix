#!/usr/bin/perl

use warnings;
use strict;

use File::Path qw(make_path);

my $builds={
	linux => [
		"386",
		"amd64",
		"arm",
		"arm64",
	],
	darwin => [
		"386",
		"amd64",
	],
	freebsd => [
		"386",
		"amd64",
	],
	netbsd => [
		"386",
		"amd64",
	],
	openbsd => [
		"386",
		"amd64",
	],
	plan9 => [
		"386",
		"amd64",
	],
	solaris => [
		"amd64",
	],
	windows => [
		"386",
		"amd64",
	],

};

# Setup govv
system('go get -u -v github.com/ahmetb/govvv');

# Create target dir
make_path('target');

foreach my $goos (sort {$a cmp $b} keys(%$builds)){
	foreach my $goarch (@{$builds->{$goos}}){
		# Read version
		open(my $fh, '<', 'VERSION');
		my $ver=<$fh>;
		chomp $ver;
		close($fh);

		print(".,::( Building version $ver for $goos-$goarch )::,.",$/);
		my $cmd=<<EOF;
export GOOS=$goos
export GOARCH=$goarch
go build -a -ldflags "\$\(govvv -flags\)" -o target/icinga-gonotify-matrix-v$ver-\$\{GOOS\}-\$\{GOARCH\} icinga-gonotify-matrix.go
EOF
		# Debug
		#print $cmd,$/;
		system($cmd)
	}
}
