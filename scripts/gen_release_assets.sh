#!/bin/bash

#CI_PROJECT_NAMESPACE=olegfiksel
#CI_PROJECT_NAME=icinga-gonotify-matrix
#CI_COMMIT_TAG=v2.1.0

if [ -z ${CI_COMMIT_TAG+x} ]; then
	echo "CI_COMMIT_TAG is unset, exiting."
	exit 0
else
	echo "CI_COMMIT_TAG is set to '$CI_COMMIT_TAG', continuing."
fi

echo "# Assets"
echo "
* [matrix-host-notification.sh](https://gitlab.com/olegfiksel/icinga-gonotify-matrix/-/raw/${CI_COMMIT_TAG}/icinga1/matrix-host-notification.sh?inline=false)
* [matrix-service-notification.sh](https://gitlab.com/olegfiksel/icinga-gonotify-matrix/-/raw/${CI_COMMIT_TAG}/icinga1/matrix-service-notification.sh?inline=false)
* [config-sample.yaml](https://gitlab.com/olegfiksel/icinga-gonotify-matrix/-/raw/${CI_COMMIT_TAG}/config-sample.yaml?inline=false)
"
for i in $(find target/ -type f | sort); do
	echo "* [${i}](https://gitlab.com/api/v4/projects/${CI_PROJECT_NAMESPACE}%2F${CI_PROJECT_NAME}/jobs/artifacts/${CI_COMMIT_TAG}/raw/${i}?job=release)"
done