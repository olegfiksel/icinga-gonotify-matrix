#!/bin/bash

if [[ -n "$1" && -n "$2" ]]; then
	# Both $1 and $2 are set
	goos=$1
	goarch=$2
	docker run --rm -ti \
		-e GOOS=$goos \
		-e GOARCH=$goarch \
		-v  ${PWD}:/work \
		-w /work \
		golang:1.14 \
		sh -c '
			export VER=`cat VERSION`
			echo ".,::( Building version ${VER} for ${GOOS}-${GOARCH} )::,."
			echo ""
			GOOS="" GOARCH="" go get -u -v github.com/ahmetb/govvv && \
				mkdir -p target && \
				go build -a -ldflags "$(govvv -flags)" -o target/icinga-gonotify-matrix-${GOOS}-${GOARCH} icinga-gonotify-matrix.go
		'
fi


