package main

import (
	"bytes"
	"errors"
	"flag"
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"io"
	"io/ioutil"
	"maunium.net/go/mautrix"
	"os"
	"regexp"
)

// Following variables will be statically linked at the time of compiling
// Source: https://oddcode.daveamit.com/2018/08/17/embed-versioning-information-in-golang-binary/

// GitCommit holds short commit hash of source tree
var GitCommit string

// GitBranch holds current branch name the code is built off
var GitBranch string

// GitState shows whether there are uncommitted changes
var GitState string

// GitSummary holds output of git describe --tags --dirty --always
var GitSummary string

// BuildDate holds RFC3339 formatted UTC date (build time)
var BuildDate string

// Version holds contents of ./VERSION file, if exists, or the value passed via the -version option
var Version string

// Config file structure
type Config struct {
	ServerURL   string `yaml:"server_url" validate:"required,url"`
	Room        string `yaml:"room" validate:"required,matrix_room"`
	AccessToken string `yaml:"access_token" validate:"required,min=30"`
}

// Params are Commandline arguments
type Params struct {
	ConfigFile  string
	MessageType string
}

func main() {
	fmt.Print(printVersion(Version, GitCommit, GitBranch, GitState, GitSummary, BuildDate))

	var err error

	logLevel := setLogLevel(os.LookupEnv("LOG_LEVEL"))
	logrus.SetLevel(logLevel)

	// Parse command line flags
	params, err := parseFlags(os.Args[0], os.Args[1:])
	if err == flag.ErrHelp {
		os.Exit(2)
	}

	if err != nil {
		logrus.Fatal(err)
	}

	config, err := parseConfig(params.ConfigFile)
	if err != nil {
		logrus.Fatal(err)
	}

	err = validateConfig(config)
	if err != nil {
		logrus.Fatal(err)
	}

	text, err := readMessage(os.Stdin)
	if err != nil {
		logrus.Fatal(err)
	}

	// Create Matrix client
	client, err := mautrix.NewClient(config.ServerURL, "", config.AccessToken)
	if err != nil {
		logrus.Fatal(err)
	}

	// Send message
	logrus.Info("Sending mesage...")
	eventID, err := sendMessage(client, config.Room, params.MessageType, text)
	if err != nil {
		logrus.Fatal(err)
	}
	logrus.Info("Message sent. EventID:", eventID)
}

func printVersion(Version string, GitCommit string, GitBranch string, GitState string, GitSummary string, BuildDate string) string {
	return fmt.Sprintf(`
   Icinga GoNotify [Matrix]

----==[ Version: %s ]=-------
GitCommit: %s
GitBranch: %s
GitState: %s
GitSummary: %s
BuildDate: %s
--------------------------------
`, Version, GitCommit, GitBranch, GitState, GitSummary, BuildDate)
}

func setLogLevel(lvl string, ok bool) logrus.Level {
	// LOG_LEVEL not set, let's default to info
	if !ok {
		lvl = "info"
	}
	// parse string, this is built-in feature of logrus
	ll, err := logrus.ParseLevel(lvl)
	// if log level cannot be parsed set it to warning, just in case
	if err != nil {
		ll = logrus.WarnLevel
	}
	return ll
}

func parseFlags(progname string, args []string) (Params, error) {
	var err error
	flags := flag.NewFlagSet(progname, flag.ContinueOnError)
	var params Params

	flags.StringVar(&params.ConfigFile, "c", "", "Config file")
	flags.StringVar(&params.MessageType, "m", "m.text", "Message type")
	flags.Usage = func() {
		fmt.Println("Usage:\nEnvironment variables:\nLOG_LEVEL={debug,info,warning,error}")
		flags.PrintDefaults()
	}
	err = flags.Parse(args)
	if err != nil {
		return params, err
	}
	if params.ConfigFile == "" {
		return params, errors.New("Config file not specified")
	}
	return params, nil
}

func readMessage(reader io.Reader) (string, error) {
	var err error
	logrus.Debug("Reading message from STDIN")
	buf, err := ioutil.ReadAll(reader)
	logrus.Debug("Done reading message from STDIN")
	if err != nil {
		return "", err
	}
	text := string(buf)
	if text == "" {
		return text, errors.New("empty message")
	}
	return text, nil
}

// Interface for mocking the file read
var fileReader = ioutil.ReadFile

// FakeReadFiler is used as a faked filesystem
type FakeReadFiler struct {
	Str string
}

// ReadFile is used to mock fake file
func (f FakeReadFiler) ReadFile(filename string) ([]byte, error) {
	buf := bytes.NewBufferString(f.Str)
	return ioutil.ReadAll(buf)
}

func parseConfig(configFilename string) (*Config, error) {
	logrus.Debug("Reading config file ", configFilename)
	configRaw, err := fileReader(configFilename)
	if err != nil {
		return nil, err
	}
	logrus.Debug("Done reading config file ", configFilename)

	logrus.Debug("Parsing config file ", configFilename)
	config := Config{}
	err = yaml.Unmarshal(configRaw, &config)
	if err != nil {
		return nil, err
	}
	logrus.Debug("Done parsing config file ", configFilename)
	return &config, nil
}

func validateConfig(config *Config) error {
	var err error

	logrus.Debug("Validating config file")
	// validate content of the config file
	validate := validator.New()

	// custom validator for matrix room
	_ = validate.RegisterValidation("matrix_room", func(fl validator.FieldLevel) bool {
		matched, _ := regexp.Match(`^[!#][^:]+:[\w\d-]+`, []byte(fl.Field().String()))
		return matched
	})

	// returns nil or ValidationErrors ( []FieldError )
	err = validate.Struct(config)
	if err != nil {
		for _, e := range err.(validator.ValidationErrors) {
			logrus.Error(e)
		}
		return err
	}
	logrus.Debug("Done validating config file")
	return nil
}

type matrixClient interface {
	SendMessageEvent(roomID string, eventType mautrix.EventType, contentJSON interface{}, extra ...mautrix.ReqSendEvent) (resp *mautrix.RespSendEvent, err error)
}

func sendMessage(client matrixClient, room string, msgType string, text string) (string, error) {
	var err error
	var resp *mautrix.RespSendEvent
	eventType := mautrix.NewEventType("m.room.message")
	resp, err = client.SendMessageEvent(room, eventType, mautrix.Content{
		MsgType:       mautrix.MessageType(msgType),
		Format:        "org.matrix.custom.html",
		FormattedBody: text,
		Body:          text,
	})
	if err != nil {
		return "", err
	}
	return resp.EventID, err
}
