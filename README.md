[![Go Report Card](https://goreportcard.com/badge/gitlab.com/olegfiksel/icinga-gonotify-matrix/badge/gitlab.com/olegfiksel/icinga-gonotify-matrix)](https://goreportcard.com/report/gitlab.com/olegfiksel/icinga-gonotify-matrix/badge/gitlab.com/olegfiksel/icinga-gonotify-matrix) [![codecov](https://codecov.io/gl/olegfiksel/icinga-gonotify-matrix/branch/master/graph/badge.svg)](https://codecov.io/gl/olegfiksel/icinga-gonotify-matrix) [![Matrix Channel](https://img.shields.io/matrix/icinga-gonotify-matrix:fiksel.info.svg?label=%23icinga-gonotify-matrix%3Afiksel.info&logo=matrix&server_fqdn=matrix.fiksel.info)](https://matrix.to/#/#icinga-gonotify-matrix:fiksel.info)

# Icinga GoNotify Matrix

Send notifications from Icinga(1,2) to a Matrix room.

![Icinga1 notification](images/screenshot01.png)
![Icinga1 notification](images/screenshot02.png)
![Icinga1 notification](images/screenshot03.png)
![Icinga1 notification](images/screenshot04.png)

# Prerequisites

## Debian/Ubuntu

* `apt-get install -y ca-certificates`

## CentOS/RHEL

* Nothing 🙂

# Getting access token

* `curl -XPOST -d '{"type": "m.login.password", "user": "username", "password": "MyVerySecurePassword"}' "https://server.domain.com/_matrix/client/r0/login"`

# Creating config

* `cp config-sample.yaml my-config.yaml`
* `vim my-config.yaml`

# Usage

```
# icinga-gonotify-matrix -h
Environment variables:
LOG_LEVEL={debug,info,warning,error}

  -c string
        Config file
  -m string
        Message type (default "m.text")
```

# Configuration

## Icinga 1

### Download the scripts

* Create directory on your Icinga host `mkdir -p /opt/icinga-gonotify-matrix/`
* Download the following files to this new directory
  * Download the binary for your platform [latest version](https://gitlab.com/api/v4/projects/olegfiksel%2Ficinga-gonotify-matrix/jobs/artifacts/latest/raw/target/icinga-gonotify-matrix-linux-amd64?job=release)
  * Download the template scripts
    * [matrix-host-notification.sh](../-/raw/latest/icinga1/matrix-host-notification.sh?inline=false)
    * [matrix-service-notification.sh](../-/raw/latest/icinga1/matrix-service-notification.sh?inline=false)
    * [config-sample.yaml](../-/raw/latest/config-sample.yaml?inline=false)
* Create the config
  * `cd /opt/icinga-gonotify-matrix/ && cp -v config-sample.yaml config.yaml`
  * Edit `config.yaml` and fill it with your credentials

# Creating Icinga (1) notification command

Add the following to your `command.cfg`:

```
################################################################################
# NOTIFICATIONS BY MATRIX
################################################################################

define command{
	command_name	notify-host-by-matrix
	command_line    /opt/icinga-gonotify-matrix/matrix-host-notification.sh -d '$LONGDATETIME$' -l '$HOSTADDRESS$' -n '$HOSTNAME$' -o '$HOSTOUTPUT$' -s '$HOSTSTATE$' -t '$NOTIFICATIONTYPE$' -m /opt/icinga-gonotify-matrix/icinga-gonotify-matrix -x /opt/icinga-gonotify-matrix/config.yaml
	}

define command{
	command_name	notify-service-by-matrix
	command_line    /opt/icinga-gonotify-matrix/matrix-service-notification.sh -d '$LONGDATETIME$' -e '$SERVICEDESC$' -l '$HOSTADDRESS$' -n '$HOSTALIAS$' -o '$SERVICEOUTPUT$\n$LONGSERVICEOUTPUT$' -s '$SERVICESTATE$' -t '$NOTIFICATIONTYPE$' -u '$SERVICEDESC$' -m /opt/icinga-gonotify-matrix/icinga-gonotify-matrix -x /opt/icinga-gonotify-matrix/config.yaml
	}
```

## Icinga 2

TODO
