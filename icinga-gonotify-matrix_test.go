package main

import (
	"bytes"
	"errors"
	"github.com/sirupsen/logrus"
	"maunium.net/go/mautrix"
	"reflect"
	"testing"
)

type mockMatrixClientSucc struct{}

func (m *mockMatrixClientSucc) SendMessageEvent(roomID string, eventType mautrix.EventType, contentJSON interface{}, extra ...mautrix.ReqSendEvent) (resp *mautrix.RespSendEvent, err error) {
	return &mautrix.RespSendEvent{
		EventID: "test-event-id",
	}, nil
}

func Test_SendMessageSuccess(t *testing.T) {
	client := &mockMatrixClientSucc{}
	eventID, _ := sendMessage(client, "test-room", "m.text", "test message text")
	if eventID != "test-event-id" {
		t.Errorf("Expected %s but got %s", "test-event-id", eventID)
	}
}

type mockMatrixClientErr struct{}

func (m *mockMatrixClientErr) SendMessageEvent(roomID string, eventType mautrix.EventType, contentJSON interface{}, extra ...mautrix.ReqSendEvent) (resp *mautrix.RespSendEvent, err error) {
	return &mautrix.RespSendEvent{
		EventID: "test-event-id",
	}, errors.New("Test error")
}

func Test_SendMessageErr(t *testing.T) {
	client := &mockMatrixClientErr{}
	_, err := sendMessage(client, "test-room", "m.text", "test message text")
	if err == nil {
		t.Errorf("Seem that func sendMessage didn't forward the err")
	}
}

func Test_readMessage(t *testing.T) {
	var tests = []struct {
		description string
		stdin       string
		result      string
		returnError bool
	}{
		{"Oneliner mesage", "Oneliner-mesage", "Oneliner-mesage", false},
		{"Multi-line message", "Multiline\nmessage", "Multiline\nmessage", false},
		{"Only newline", "\n", "\n", false},
		{"Empty message", "", "", true},
	}

	var stdin bytes.Buffer
	for _, tt := range tests {
		t.Run(tt.description, func(t *testing.T) {
			stdin.Write([]byte(tt.stdin))
			text, err := readMessage(&stdin)
			if tt.returnError {
				if err == nil {
					t.Errorf("readMessage did not return an error, when it should")
				}
			} else {
				if err != nil {
					t.Errorf("readMessage returned an error \"%s\" (it should not)", err)
				}
				if text != tt.result {
					t.Errorf("readMessage didn't return the expected text but %s", text)
				}
			}
		})
	}
}

func Test_validateConfig(t *testing.T) {
	var tests = []struct {
		description    string
		config         Config
		expectingError bool
	}{
		{"Good config", Config{
			ServerURL:   "https://server.domain.com:8448",
			Room:        "!fsdfFDSfDfsDfjelYp:server.domain.com",
			AccessToken: "VERY_LOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONG_ACCESS_TOKEN",
		}, false},
		{"Empty config", Config{}, true},
		{"Wrong server URL", Config{
			ServerURL:   "server.domain.com",
			Room:        "!fsdfFDSfDfsDfjelYp:server.domain.com",
			AccessToken: "VERY_LOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONG_ACCESS_TOKEN",
		}, true},
		{"Wrong room", Config{
			ServerURL:   "https://server.domain.com:8448",
			Room:        "blahblah",
			AccessToken: "VERY_LOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONG_ACCESS_TOKEN",
		}, true},
		{"Wrong Access Token", Config{
			ServerURL:   "https://server.domain.com:8448",
			Room:        "!fsdfFDSfDfsDfjelYp:server.domain.com",
			AccessToken: "SHORT_ACCESS_TOKEN",
		}, true},
	}

	for _, tt := range tests {
		t.Run(tt.description, func(t *testing.T) {
			err := validateConfig(&tt.config)
			if tt.expectingError {
				if err == nil {
					t.Errorf("validateConfig did not return an error, when it should")
				}
			} else {
				if err != nil {
					t.Errorf("validateConfig returned an error \"%s\" (it should not)", err)
				}
			}
		})
	}
}

func Test_parseFlags(t *testing.T) {
	var tests = []struct {
		description    string
		args           []string
		params         Params
		expectingError bool
	}{
		{
			"Correct params",
			[]string{"-c", "/some/path/some_file.yaml", "-m", "m.notice"},
			Params{
				ConfigFile:  "/some/path/some_file.yaml",
				MessageType: "m.notice",
			},
			false,
		},
		{
			"Missing config file",
			[]string{"-m", "m.notice"},
			Params{},
			true,
		},
		{
			"Missing message type",
			[]string{"-c", "/some/path/some_file.yaml"},
			Params{
				ConfigFile:  "/some/path/some_file.yaml",
				MessageType: "m.text",
			},
			false,
		},
		{
			"Incomplete config file",
			[]string{"-c"},
			Params{
				ConfigFile:  "",
				MessageType: "m.text",
			},
			true,
		},
		{
			"Incomplete message type",
			[]string{"-c", "/some/path/some_file.yaml", "-m"},
			Params{
				ConfigFile:  "/some/path/some_file.yaml",
				MessageType: "m.text",
			},
			true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.description, func(t *testing.T) {
			params, err := parseFlags("test-progname", tt.args)
			if tt.expectingError {
				if err == nil {
					t.Errorf("parseFlags did not return an error, when it should")
				}
			} else {
				if err != nil {
					t.Errorf("parseFlags returned an error \"%s\" (it should not)", err)
				} else {
					if !reflect.DeepEqual(&params, &tt.params) {
						t.Errorf("parseFlags returned %+v, want %+v", params, tt.params)
					}
				}
			}
		})
	}
}

func Test_parseConfig(t *testing.T) {
	var tests = []struct {
		description    string
		payload        string
		expectedResult Config
		expectingError bool
	}{
		{
			"Correct config",
			`
                server_url: "https://server.domain.com:8448"
                room: "!fsdfFDSfDfsDfjelYp:server.domain.com"
                access_token: "ACCESS_TOKEN"
            `,
			Config{
				ServerURL:   "https://server.domain.com:8448",
				Room:        "!fsdfFDSfDfsDfjelYp:server.domain.com",
				AccessToken: "ACCESS_TOKEN",
			},
			false,
		},
		{
			"Garbage",
			"KJHlkjh80y089gUYGVIUyg98y9=134324#@$@#43223423423",
			Config{},
			true,
		},
		{
			"Empty",
			"",
			Config{},
			false,
		},
		{
			"Partial",
			`server_url: "https://server.domain.com:8448"`,
			Config{ServerURL: "https://server.domain.com:8448"},
			false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.description, func(t *testing.T) {
			fakeReader := FakeReadFiler{Str: tt.payload}
			fileReader = fakeReader.ReadFile
			config, err := parseConfig("mocked")
			if tt.expectingError {
				if err == nil {
					t.Errorf("parseConfig did not return an error (when it should) but returned instead %+v", config)
				}
			} else {
				if err != nil {
					t.Errorf("parseConfig returned an error \"%s\" (it should not)", err)
				} else {
					if !reflect.DeepEqual(config, &tt.expectedResult) {
						t.Errorf("parseFlags returned %+v, want %+v", config, tt.expectedResult)
					}
				}
			}
		})
	}
}

func Test_setLogLevel(t *testing.T) {
	var tests = []struct {
		description    string
		envVarValue    string
		envVarExist    bool
		expectedResult logrus.Level
	}{
		{
			"Warning log level",
			"warning",
			true,
			logrus.WarnLevel,
		},
		{
			"Unknown log level provided",
			"unknown log level",
			true,
			logrus.WarnLevel,
		},
		{
			"Env var is not set",
			"",
			false,
			logrus.InfoLevel,
		},
	}
	for _, tt := range tests {
		t.Run(tt.description, func(t *testing.T) {
			ll := setLogLevel(tt.envVarValue, tt.envVarExist)
			if ll != tt.expectedResult {
				t.Errorf("setLogLevel returned %+v, want %+v", ll, tt.expectedResult)
			}
		})
	}
}

func Test_printVersion(t *testing.T) {
	var tests = []struct {
		description     string
		inputVersion    string
		inputGitCommit  string
		inputGitBranch  string
		inputGitState   string
		inputGitSummary string
		inputBuildDate  string
		expectedOutput  string
	}{
		{
			"Data provided",
			"1.0",
			"4627742",
			"master",
			"clean",
			"4627742",
			"2020-05-10T21:05:53Z",
			`
   Icinga GoNotify [Matrix]

----==[ Version: 1.0 ]=-------
GitCommit: 4627742
GitBranch: master
GitState: clean
GitSummary: 4627742
BuildDate: 2020-05-10T21:05:53Z
--------------------------------
`,
		},
		{
			"Data not provided",
			"",
			"",
			"",
			"",
			"",
			"",
			`
   Icinga GoNotify [Matrix]

----==[ Version:  ]=-------
GitCommit: 
GitBranch: 
GitState: 
GitSummary: 
BuildDate: 
--------------------------------
`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.description, func(t *testing.T) {
			ll := printVersion(tt.inputVersion, tt.inputGitCommit, tt.inputGitBranch, tt.inputGitState, tt.inputGitSummary, tt.inputBuildDate)
			if ll != tt.expectedOutput {
				t.Errorf("printVersion returned %+v, want %+v", ll, tt.expectedOutput)
			}
		})
	}
}
