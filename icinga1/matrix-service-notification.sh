#!/bin/bash
# Based on original E-Mail Icinga2 notification

PROG="`basename $0`"
ICINGA2HOST="`hostname`"

## Function helpers
Usage() {
cat << EOF

Required parameters:
  -d LONGDATETIME (\$icinga.long_date_time\$)
  -e SERVICENAME (\$service.name\$)
  -l HOSTNAME (\$host.name\$)
  -n HOSTDISPLAYNAME (\$host.display_name\$)
  -o SERVICEOUTPUT (\$service.output\$)
  -s SERVICESTATE (\$service.state\$)
  -t NOTIFICATIONTYPE (\$notification.type\$)
  -u SERVICEDISPLAYNAME (\$service.display_name\$)
  -m MATRIXNOTIFYBIN (\$notification_matrix_notify_binary\$)
  -x MATRIXCONFIG (\$notification_matrix_config\$)

Optional parameters:
  -4 HOSTADDRESS (\$address\$)
  -6 HOSTADDRESS6 (\$address6\$)
  -b NOTIFICATIONAUTHORNAME (\$notification.author\$)
  -c NOTIFICATIONCOMMENT (\$notification.comment\$)
EOF
}

Help() {
  Usage;
  exit 0;
}

Error() {
  if [ "$1" ]; then
    echo $1
  fi
  Usage;
  exit 1;
}

## Main
while getopts 4:6:b:c:d:e:h:l:n:o:s:t:u:m:x: opt
do
  case "$opt" in
    4) HOSTADDRESS=$OPTARG ;;
    6) HOSTADDRESS6=$OPTARG ;;
    b) NOTIFICATIONAUTHORNAME=$OPTARG ;;
    c) NOTIFICATIONCOMMENT=$OPTARG ;;
    d) LONGDATETIME=$OPTARG ;; # required
    e) SERVICENAME=$OPTARG ;; # required
    h) Help ;;
    l) HOSTNAME=$OPTARG ;; # required
    n) HOSTDISPLAYNAME=$OPTARG ;; # required
    o) SERVICEOUTPUT=$OPTARG ;; # required
    s) SERVICESTATE=$OPTARG ;; # required
    t) NOTIFICATIONTYPE=$OPTARG ;; # required
    u) SERVICEDISPLAYNAME=$OPTARG ;; # required
	m) MATRIXNOTIFYBIN=$OPTARG ;; # required
    x) MATRIXCONFIG=$OPTARG ;; # required
   \?) echo "ERROR: Invalid option -$OPTARG" >&2
       Error ;;
    :) echo "Missing option argument for -$OPTARG" >&2
       Error ;;
    *) echo "Unimplemented option: -$OPTARG" >&2
       Error ;;
  esac
done

shift $((OPTIND - 1))

if [ -z "`which $MATRIXNOTIFYBIN`" ] ; then
  echo "$MATRIXNOTIFYBIN not found."
  exit 1
fi

## Check required parameters (TODO: better error message)
if [ ! "$LONGDATETIME" ] \
|| [ ! "$HOSTNAME" ] || [ ! "$HOSTDISPLAYNAME" ] \
|| [ ! "$SERVICENAME" ] || [ ! "$SERVICEDISPLAYNAME" ] \
|| [ ! "$SERVICEOUTPUT" ] || [ ! "$SERVICESTATE" ] \
|| [ ! "$NOTIFICATIONTYPE" ]; then
  Error "Requirement parameters are missing."
fi

## Build the notification message

if [ "$SERVICESTATE" = "CRITICAL" ]
then
        s_color=#FF5566
        ico="🔥"

elif [ "$SERVICESTATE" = "WARNING" ]
then
        s_color=#FFAA44
        ico="⚠"

elif [ "$SERVICESTATE" = "UNKNOWN" ]
then
        s_color=#90A4AE
        ico="?"

elif [ "$SERVICESTATE" = "DOWN" ]
then
        s_color=#FF5566
        ico="!"

#else [ "$SERVICESTATE" = "OK" ]
#then
else
        s_color=#44BB77
        ico="✓"
fi

NOTIFICATION_MESSAGE="<font color='$s_color'>$ico Service Monitoring on $ICINGA2HOST</font><br>
<u>$NOTIFICATIONTYPE</u> on <strong>$HOSTDISPLAYNAME</strong><br>
<strong>$SERVICEDISPLAYNAME</strong> is <font color='$s_color'>$SERVICESTATE!</font><br>
When: $LONGDATETIME<br>
"

## Check whether IPv4 / IPv6 was specified.
if [ -n "$HOSTADDRESS" ] && [ -n "$HOSTADDRESS6" ] ; then
  NOTIFICATION_MESSAGE="$NOTIFICATION_MESSAGE
Host: $HOSTNAME<br>
IPv4: $HOSTADDRESS / <strong>IPv6:</strong> $HOSTADDRESS6<br>"
elif [ -n "$HOSTADDRESS" ] ; then
  NOTIFICATION_MESSAGE="$NOTIFICATION_MESSAGE
Host: $HOSTNAME / $HOSTADDRESS<br>"
elif [ -n "$HOSTADDRESS6" ] ; then
  NOTIFICATION_MESSAGE="$NOTIFICATION_MESSAGE
Host: $HOSTNAME / $HOSTADDRESS6<br>"
fi

## Check whether author and comment was specified.
if [ -n "$NOTIFICATIONCOMMENT" ] ; then
  NOTIFICATION_MESSAGE="$NOTIFICATION_MESSAGE
Comment: <font color='#3333ff'>$NOTIFICATIONCOMMENT</font> by <font color='#c47609'>$NOTIFICATIONAUTHORNAME</font><br>"
fi

## Additional Info
if [ -n "$SERVICEOUTPUT" ]; then
    NOTIFICATION_MESSAGE="$NOTIFICATION_MESSAGE
Additional Info: <pre><code>$SERVICEOUTPUT</code></pre>"
fi

while read line; do
  message="${message}\n${line}"
done <<< $NOTIFICATION_MESSAGE

BODY="${message}"

/usr/bin/printf "%b" "$NOTIFICATION_MESSAGE" | $MATRIXNOTIFYBIN -c "$MATRIXCONFIG"
